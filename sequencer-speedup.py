
bl_info = {
    "name": "Speed Up",
    "blender": (3, 1, 0),
    "category": "Sequencer",
}

import bpy

class SEQUENCER_OT_speedup(bpy.types.Operator):
   """Speed Up Script"""      # Use this as a tooltip for menu items and buttons.
   bl_idname = "sequencer.speedup"        # Unique identifier for buttons and menu items to reference.
   bl_label = "Speed Up"         # Display name in the interface.
   bl_options = {'REGISTER', 'UNDO'}  # Enable undo for the operator.

   speedScale: bpy.props.IntProperty(name="Speed Scale", default=4, min=1, max=8)
   volume: bpy.props.FloatProperty(name="Volume", default=0.25, min=0, max=1)

   def computeDuration(self, strip, duration, speedScale):
      if duration == None:
         duration = int(strip.frame_final_duration / speedScale)
      return duration

   def applyToSound(self, sound, speedScale, volume, duration):
      duration = self.computeDuration(sound, duration, speedScale)
      sound.pitch  = speedScale
      sound.volume = volume
      sound.frame_final_duration = duration

   def deselectEverything(self, context):
      for strip in context.selected_editable_sequences:
         strip.select = False

   def addSpeedEffect(self, context, movie, speedScale):
      self.deselectEverything(context)
      movie.select = True
      bpy.ops.sequencer.effect_strip_add(type='SPEED')
      newEffect = context.selected_editable_sequences[0]
      newEffect.speed_control = 'MULTIPLY'
      newEffect.speed_factor = speedScale

   def applyToMovie(self, context, movie, speedScale, duration):
      duration = self.computeDuration(movie, duration, speedScale)
      self.addSpeedEffect(context, movie, speedScale)
      movie.frame_final_duration = duration



   def applyToMeta(self, context, meta, speedScale, volume, duration):
      duration = self.computeDuration(meta, duration, speedScale)
      offset = int(meta.frame_offset_start * ((speedScale - 1) / speedScale))
      bpy.ops.sequencer.meta_toggle()
      for strip in context.sequences:
         match strip.type:
            case 'MOVIE':
               self.addSpeedEffect(context, strip, speedScale)
               strip.frame_start += offset
            case 'SOUND':
               strip.pitch  = speedScale
               strip.volume = volume
            # TODO: Not sure what to do about nested metas
      bpy.ops.sequencer.meta_toggle()
      meta.frame_final_duration = duration
      

   def applyToList(self, context, list, speedScale, volume, duration):
      for strip in list:
         match strip.type:
            case 'MOVIE':
               self.applyToMovie(context, strip, speedScale, duration)
            case 'SOUND':
               self.applyToSound(strip, speedScale, volume, duration)
            case 'META':
               self.applyToMeta(context, strip, speedScale, volume, duration)
      

   def execute(self, context):
      selected = context.selected_editable_sequences
      self.applyToList(context, selected, self.speedScale, self.volume, None)
      return {'FINISHED'}


def menu_func(self, context):
   self.layout.operator(SEQUENCER_OT_speedup.bl_idname, text="Speed Up")

def register():
   #print("Hello World Register")
   bpy.utils.register_class(SEQUENCER_OT_speedup)
   bpy.types.SEQUENCER_MT_context_menu.prepend(menu_func)
   #bpy.types.SEQUENCER_MT_strip.append(menu_func)
def unregister():
   #print("Hello World Unregister")
   bpy.utils.unregister_class(SEQUENCER_OT_speedup)
   bpy.types.SEQUENCER_MT_context_menu.remove(menu_func)




if __name__ == "__main__":
    register()

